## 프로젝트 소개


URL을 입력받아 짧게 줄여주고, Shortening된 URL을 입력하면 원래 URL로 리다이렉트하는 URL Shortening Service

~~~
 - 문제해결 (Shortening URL 로직)

1. https://bitly.com/ 사이트 참고하여 UI 개발
2. 대문자+소문자+숫자 조합 -> 위치 랜덤하게 섞은 후 URL Shortening Key 7 Character로 DB에 저장
3. 같은 URL을 재 입력해도 Shortening Key는 다르게 추출 되어야 하므로 DB에 저장, URL 리다이렉트 시 비교하는 로직으로 구현
   
 - 저장된 shorten Table Data
  > longurl  : https://en.wikipedia.org/wiki/URL_shortening
  > shorturl : http://localhost/SWp2et4
~~~

 

---




## 개발환경
~~~
 - Bitbucket 소스 다운로드 (프로젝트명 : ShortenTest)
 
   > 왼쪽 메뉴 Downloads 클릭 -> Branches 탭 클릭 -> master 다운로드 
   > https://bitbucket.org/sokkim/shortentest/downloads/?tab=branches 

 - SpringMVC + MAVEN + Tomcat + MyBatis + BootStrap + Mysql 구성

 - 형상관리 
1. Git
2. Bitbucket
3. SourceTree

 - 버전정보
1. Spring 5.1.1
2. JDK 1.8
3. TOMCAT 8.5

 - Tomcat
1. Port : 80
2. Path : /
3. Module : springurlshorten  

 - MYSQL DB 계정정보
1. Hostname: 127.0.0.1
2. Schema : test
3. Port: 3306
4. Username: test
5. Password: password1!
~~~
    
---




## 프로젝트 실행

~~~
 - 웹 접속
 
1. 다은로드 받은 프로젝트를 maven 빌드 후 Tomcat 수행
2. 웹브라우저에 http://localhost 접속
3. 변경할 URL 입력 후 Shorten 버튼 클릭
4. Shortening URL 복사해서 웹브라우저에 접속
5. 존재하는 페이지면 원래 URL로, 없는 URL이면 error 페이지로 이동
6. Tomcat 실행시 java.lang.ClassNotFoundException: org.springframework.web.context.ContextLoaderListener 에러가 날경우
  
  <이클립스의 경우 아래 처럼 설정>
  프로젝트 > properties > Deployment Assembly  -> add 눌러서 Java Build Path Entries > Maven Dependencies 선택 > apply



 - Junit Test

1. MockMvc를 이용해서 구현
2. testShortenRedirect() 메소드 테스트시 key값만 입력
3. testShorten() 메소드 테스트시 변경할 longurl 값만 세팅
~~~

---



## DataBase 생성 및 사용자 권한부여

~~~
1. DataBase 생성
   > create database test;

2. User 생성
   > create user test@'%' identified by 'password1!';
   
3. 권한 부여
   > grant all privileges on test.* to 'test'@'%';
   > flush privileges;
   
4. 테이블 생성

CREATE TABLE `shorten` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `longurl` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shorturl` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
~~~