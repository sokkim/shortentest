<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="${contextPath}/resources/image/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<title>URL Shortening Service</title>
	<script type="text/javascript">
	$(function() {		
		$("#btnShorten").on("click", function(e) {
			e.preventDefault();
			var $form = $("#frm");

			$form.submit();
		});
	});
	</script>
</head>
<body>
	<form name="frm" id="frm" action="/shortenCreate" method="post">
	<div class="card" style="width: 40rem;">
		<div class="card-header text-center h2">URL Shortening Service</div>
		<div class="card-body">
			<input type="text" class="form-control" name="longUrl" id="longUrl" placeholder="Long URL Input" value="${map.longUrl}">
			<p></p>
			<p class="text-center"><button type="button" class="btn btn-secondary btn-lg" id="btnShorten">Shorten</button></p>
			<input type="text" class="form-control" name="shortUrl" id="shortUrl" placeholder="Shorten URL" value="${map.shortUrl}" readOnly>
		</div>
		<c:if test="${not empty urls}">
		<div>
		<table class="table">		  
		  <tbody>
		    <c:forEach var="row" items="${urls}">
		    <tr>
		      <td>${row.longUrl}</td>
		      <td>${row.shortUrl}</td>
		    </tr>
		   </c:forEach>
		  </tbody>
		</table>
		</div>
		</c:if>
	</div>
	</form>
</body>
</html>
