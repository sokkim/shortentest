<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<html>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="${contextPath}/resources/image/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
	<div class="card text-center">
		<div class="card-header h2">페이지가 존재하지 않습니다.</div>
		<div class="card-body">
			<h5 class="card-title"></h5>
			<p class="card-text"></p>
			<a href="${contextPath}/" class="btn btn-primary">Go URL Shortening Service</a>
		</div>
		<div class="card-footer text-muted"></div>
	</div>
</body>
</html>