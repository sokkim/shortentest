package com.my.coding.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.my.coding.common.Constant;
import com.my.coding.model.dto.ShortenDTO;
import com.my.coding.service.ShortenService;

@Controller
public class ShortenController {
	
	private static final Logger logger = LoggerFactory.getLogger(ShortenController.class);
	
	@Autowired
	ShortenService shortenService;
	
	/**
	 * 기본 url
	 * @return
	 */
	@RequestMapping("/")
	public ModelAndView main() throws Exception {
		List<ShortenDTO> urlList = shortenService.selectUrlList();
		
		ModelAndView mvn = new ModelAndView("shorten");
		mvn.addObject("urls", urlList);
		
		return mvn;
	}
	
	/**
	 * shorten url로 접속
	 * @param shorten
	 * @return
	 */
	@RequestMapping("/{shorten}")
	public String shortenRedirect(@PathVariable("shorten") String shorten) throws Exception {
		String longUrl = shortenService.selectUrl(Constant.ORIGIN_PATH+shorten);
		if ("".equals(longUrl) || longUrl == null) throw new NoSuchFieldException("Page Not Found");
		
		return "redirect:"+longUrl;
	}
	
	/**
	 * 대문자+소문자+숫자 조합 7자리
	 * @param dto
	 * @return
	 */
	@RequestMapping(value="/shortenCreate", method = RequestMethod.POST)
	public ModelAndView shorten(@ModelAttribute ShortenDTO dto) throws Exception {
		ShortenDTO resultDto = shortenService.insertUrl(dto);
		List<ShortenDTO> urlList = shortenService.selectUrlList();
		
        ModelAndView mvn = new ModelAndView("shorten");
        mvn.addObject("map", resultDto);
        mvn.addObject("urls", urlList);
        
		return mvn;
	}	
}
