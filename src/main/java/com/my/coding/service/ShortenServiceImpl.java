package com.my.coding.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.coding.common.Constant;
import com.my.coding.model.dao.ShortenDAO;
import com.my.coding.model.dto.ShortenDTO;

@Service
public class ShortenServiceImpl implements ShortenService {
	private static final Logger logger = LoggerFactory.getLogger(ShortenServiceImpl.class);

	@Autowired
	ShortenDAO shortenDAO;

	@Override
	public String selectUrl(String shorten) {
		return shortenDAO.selectUrl(shorten);
	}

	@Transactional
	@Override
	public ShortenDTO insertUrl(ShortenDTO dto) {
		if (!"".equals(dto.getLongUrl()) && dto.getLongUrl() !=null) {
			List<Character> charList = new ArrayList<Character>();

			for (char i = 65 ; i <= 90 ; i++) {
				charList.add(i);
			}
			for (char i = 97 ; i <= 122 ; i++) {
				charList.add(i);
			}
			for (char i = 48 ; i <= 57 ; i++) {
				charList.add(i);
			}

			char[] carr = new char[charList.size()];
			for (int i = 0; i < charList.size(); i++) {
				carr[i] = charList.get(i);
			}
			char[] sff = shuffle(carr);

			StringBuffer sb = new StringBuffer();
			sb.append(Constant.ORIGIN_PATH);
			for(int i = 0 ; i < 7 ; i++) {
				sb.append(sff[i]);
			}
			dto.setShortUrl(sb.toString());

			shortenDAO.insertUrl(dto);
		}
		return dto;
	}

	/**
	 * 랜덤숫자*배열사이즈의 정수값을 index 위치 이동
	 * @param carr
	 * @return
	 */
	public char[] shuffle(char[] carr) {
		for (int i = 0; i < carr.length; i++) { 
			int r = (int) (Math.random() * carr.length); 
			char tmp = carr[r];
			carr[r] = carr[i];
			carr[i] = tmp;
		} 
		return carr;
	}

	@Override
	public List<ShortenDTO> selectUrlList() {
		return shortenDAO.selectUrlList();
	}

}
