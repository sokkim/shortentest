package com.my.coding.service;

import java.util.List;

import com.my.coding.model.dto.ShortenDTO;

public interface ShortenService {
	public String selectUrl (String shorten);
	public ShortenDTO insertUrl (ShortenDTO dto);
	public List<ShortenDTO> selectUrlList ();
}
