package com.my.coding.model.dao;

import java.util.List;

import com.my.coding.model.dto.ShortenDTO;

public interface ShortenDAO {
	public String selectUrl (String shorten);
	public int insertUrl (ShortenDTO dto);
	public List<ShortenDTO> selectUrlList ();
}
