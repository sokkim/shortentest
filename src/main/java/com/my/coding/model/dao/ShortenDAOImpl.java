package com.my.coding.model.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.my.coding.model.dto.ShortenDTO;

@Repository
public class ShortenDAOImpl implements ShortenDAO {

	@Autowired
	SqlSession sqlSession;
	
	@Override
	public String selectUrl(String shorten) {
		return sqlSession.selectOne("shorten.selectUrl", shorten);
	}

	@Override
	public int insertUrl(ShortenDTO dto) {
		return sqlSession.insert("shorten.insertUrl", dto);
	}

	@Override
	public List<ShortenDTO> selectUrlList() {
		return sqlSession.selectList("shorten.selectUrlList");
	}

}
