package com.my.coding.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml"})
public class ShortenControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(ShortenControllerTest.class);
	
	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockmvc;
	
	@Before
	public void setup() {
		this.mockmvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	
	
	@Test
	public void testMain() throws Exception {
		this.mockmvc.perform(post("/"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(handler().handlerType(ShortenController.class))
		.andExpect(handler().methodName("main"));
	}
	
	@Ignore
	@Test
	public void testShortenRedirect() throws Exception {
		this.mockmvc.perform(post("/8LkbmNX"))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(handler().handlerType(ShortenController.class))
		.andExpect(handler().methodName("shortenRedirect"));
	}
	
	@Ignore
	@Test
	public void testShorten() throws Exception { 
		this.mockmvc.perform(post("/shortenCreate").param("longUrl", "https://en.wikipedia.org/wiki/URL_shortening"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(handler().handlerType(ShortenController.class))
		.andExpect(handler().methodName("shorten"));
	}
	
}
